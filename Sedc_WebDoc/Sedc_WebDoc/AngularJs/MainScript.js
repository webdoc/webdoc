﻿/// <reference path="../Scripts/angular.min.js" />

//Module
var HomeModule = angular.module("HomeModule", ['ngRoute']);

//Controllers
HomeModule.controller("HomeController", function ($scope) {
    $scope.message = "Hi";

})

//RoutConfig
HomeModule.config(function ($routeProvider, $locationProvider) {
    $routeProvider.
          when("/Home", { templateUrl: "Scripts/views/Home.html" })
          .when("/AddTransaction", { templateUrl: "Scripts/views/AddTransaction.html" })
          .otherwise({
              redirectTo: "/Home"
          })
    $locationProvider.html5Mode(true);
})


